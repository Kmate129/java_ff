package gameclub.view;

import gameclub.domain.*;
import org.javatuples.Pair;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

@Component
public class ConsoleView implements IConsoleView{
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    @Value("${application-name}")
    private String appName;

    public Pair<String,String> LogInDialog() throws IOException {
        System.out.println("Welcome to " +appName +"\n");
        System.out.println("Please log in.");
        System.out.print("Login name: ");
        String name = br.readLine();
        System.out.print("Password: ");
        String ps = br.readLine();
        return new Pair<>(name, ps);
    }

    public void LogInResult(){
        System.out.println("Login failure, bye.");
    }

    public void LogInResult(User user){
        System.out.println("Login successful. Your role: " + RolesToString(user.getRole()));
    }

    public void DataSheet(User user, List<Group> memberships, boolean header){
        if(header){
            System.out.println("Hi " + user.getName() + "\n");
        }
        Player p = (Player) user;
        System.out.println("Your games:");
        if(p.getGames().size()!=0){
            for (Game g : p.getGames()) {
                System.out.println("- " + g.getName());
            }
        }else{
            System.out.println("N/A");
        }

        System.out.println("\nGroup membership:");
        if(memberships.size()!=0){
            for (Group membership : memberships) {
                System.out.println("- " + membership.getName());
            }
        }else{
            System.out.println("N/A");
        }
    }

    public void DataSheet(User user, List<Group> memberships, Group owngroup, List<JoinRequest> joinrequests ) {
        System.out.println("Hi " + user.getName() + "\n");
        System.out.println("Your group:");
        System.out.println("- " + owngroup.getName()+"\n");
        System.out.println("Group join requests:");
        if(joinrequests.size()!=0){
            for (JoinRequest joinrequest : joinrequests) {
                System.out.println("- " + joinrequest.getPlayer().getName());
            }
        }else{
            System.out.println("N/A");
        }
        DataSheet(user, memberships, false);
    }

    public int Menu(List<Role> role) throws IOException, IllegalArgumentException {
        int menuitemCounter=1;
        ArrayList<String> items = new ArrayList<>();
        System.out.println("\nHere are the possible actions:");
        System.out.println(menuitemCounter + ". View list of all games");
        menuitemCounter++;
        items.add("VIEW");
        if(role.contains(Role.PLAYER)){
            System.out.println(menuitemCounter + ". Add my game");
            menuitemCounter++;
            items.add("ADD");
            System.out.println(menuitemCounter + ". Create Join request");
            menuitemCounter++;
            items.add("CREATE");
        }
        if(role.contains(Role.GROUPADMIN)){
            System.out.println(menuitemCounter + ". Handle Join request");
            menuitemCounter++;
            items.add("HANDLE");
        }

        System.out.println(menuitemCounter + ". Quit application\n");
        items.add("QUIT");
        System.out.println("Please choose an item: ");
        int answer =  Integer.parseInt(br.readLine());
        if(answer>=1 && answer<=menuitemCounter){
            return MenuItemConverter(items.get(answer-1));
        }else{
            throw new IllegalArgumentException("Not valid input.");
        }

    }

    public void ListGames(List<Game> games){
        for (var game: games) {
            System.out.println("- id: " + game.getId());
            System.out.println("    Name: " + game.getName());
            System.out.println("    Description: " + game.getDescription());
            System.out.println("    Minimum age: " + game.getMinimumAge());
            System.out.println("    Number of players: " + game.getNumberOfPlayers().getMin() +"-" + game.getNumberOfPlayers().getMax());
            System.out.println("    Play time: "+game.getPlayTime().getMin()+"min-"+game.getPlayTime().getMax()+"min");
        }
    }

    @Nullable
    public Game GameAdding(List<Game> games) throws IOException {
        System.out.println("Please choose a game from the following list add:");
        int i =0;
        for (; i< games.size(); i++){
            System.out.println(i+1 + ". " + games.get(i).getName());
        }
        System.out.println(i+1 + ". Back to main menu\n");
        System.out.println("Please choose game: ");
        int res = Integer.parseInt(br.readLine());
        if(res>0 && res< i+1){
            return games.get(res-1);
        }else{
            return null;
        }
    }

    @Nullable
    public Group JoinRequestCreation(List<Group> groups) throws IOException {
        System.out.println("Please choose the group you would like to join:");
        int i =0;
        for (; i< groups.size(); i++){
            System.out.println(i+1 + ". " + groups.get(i).getName());
        }
        System.out.println(i+1 + ". Back to main menu\n");

        System.out.println("Please choose group: ");
        int res = Integer.parseInt(br.readLine());
        if(res>0 && res< i+1){
            return groups.get(res-1);
        }else{
            return null;
        }
    }

    @Nullable
    public Pair<Long, JoinRequestState> HandleJoinRequest(List<User> users) throws IOException {
        System.out.println("List of players who would like to join your group. Please select player number and (A)ccept or (R)eject:");
        int i =0;
        for (; i< users.size(); i++){
            System.out.println(i+1 + ". " + users.get(i).getName());
        }
        System.out.println(i+1 + ". Back to main menu\n");

        System.out.println("Please answer: ");
        String res = br.readLine();
        StringBuilder userIdx= new StringBuilder();
        JoinRequestState state = JoinRequestState.REJECTED;
        for (int j=0; j<res.length(); j++){
            if(res.charAt(j) >='1' && res.charAt(j) <= '9'){
                userIdx.append(res.charAt(j));
            }else{
                if(res.charAt(j)=='A'){
                    state=JoinRequestState.ACCEPTED;
                }
                if(res.charAt(j)=='R'){
                    state=JoinRequestState.REJECTED;
                }
            }
        }

        if(res.equals(String.valueOf(i+1))){
            return null;
        }else{
            return new Pair<>(users.get(Integer.parseInt(userIdx.toString()) - 1).getId(), state);
        }
    }

    private String RolesToString(List<Role> roles){
        StringBuilder s = new StringBuilder();
        for (int i=0;i<roles.size();i++){
            s.append(roles.get(i));
            if(i< roles.size()-1){
                s.append(", ");
            }
        }
        return s.toString();
    }

    private int MenuItemConverter(String s){
        switch (s){
            case "VIEW":
                return 1;
            case "ADD":
                return 2;
            case "CREATE":
                return 3;
            case "HANDLE":
                return 4;
            case "QUIT":
                return 5;
            default:
                throw new IllegalArgumentException("Not valid menu item");
        }
    }
}
