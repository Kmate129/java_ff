package gameclub.view;

import gameclub.domain.*;
import org.javatuples.Pair;
import java.io.IOException;
import java.util.List;

public interface IConsoleView {
    Pair<String,String> LogInDialog() throws IOException;
    void LogInResult();
    void LogInResult(User user);
    void DataSheet(User user, List<Group> memberships, boolean header);
    void DataSheet(User user, List<Group> memberships, Group owngroup, List<JoinRequest> joinrequests );
    int Menu(List<Role> role) throws IOException;
    void ListGames(List<Game> games);
    Game GameAdding(List<Game> games) throws IOException;
    Group JoinRequestCreation(List<Group> groups) throws IOException;
    Pair<Long,JoinRequestState> HandleJoinRequest(List<User> users) throws IOException;
}