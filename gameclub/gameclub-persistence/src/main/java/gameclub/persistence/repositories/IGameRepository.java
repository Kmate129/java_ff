package gameclub.persistence.repositories;

import gameclub.domain.Game;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IGameRepository extends CrudRepository<Game, Long> {

    @Override
    List<Game> findAll();
}
