package gameclub.persistence.repositories;

import gameclub.domain.Group;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface IGroupRepository extends CrudRepository<Group, Long> {

    @Override
    List<Group> findAll();

    Optional<Group> findById(Long id);
}
