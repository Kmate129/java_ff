package gameclub.persistence.repositories;

import gameclub.domain.Player;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface IUserRepository extends CrudRepository<Player, Long> {

    @Override
    List<Player> findAll();

    Player findByLoginName(String LoginName);
}
