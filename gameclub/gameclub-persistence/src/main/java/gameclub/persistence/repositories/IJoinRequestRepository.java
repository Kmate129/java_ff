package gameclub.persistence.repositories;

import gameclub.domain.JoinRequest;
import gameclub.domain.JoinRequestId;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IJoinRequestRepository extends CrudRepository<JoinRequest, JoinRequestId> {

    @Override
    List<JoinRequest> findAll();
}
