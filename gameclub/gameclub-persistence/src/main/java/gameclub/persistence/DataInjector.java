package gameclub.persistence;

import gameclub.domain.*;
import gameclub.persistence.repositories.IGameRepository;
import gameclub.persistence.repositories.IGroupRepository;
import gameclub.persistence.repositories.IJoinRequestRepository;
import gameclub.persistence.repositories.IUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class DataInjector {

    @Autowired
    IGameRepository gameRepository;

    @Autowired
    IGroupRepository groupRepository;

    @Autowired
    IUserRepository userRepository;

    @Autowired
    IJoinRequestRepository joinRequestRepository;

    @Transactional
    public void Inject(){
        Game game1 = new Game( "Catan Telepesei", "A Catan telepesei a legtöbb társasjáték rajongónak az első lépés ami túlmutat a gyermekkori klasszikus dobok és lépek játékokon. A játék célja Catan szigetén megszerezni az uralmat…",
                10, new ArrayList<>(List.of(Category.STRATEGY)), new Limits(3,4), new Limits(60,120));
        Game game2 = new Game( "Pandemic", "Megvan benned a képesség és a bátorság ahhoz, hogy megmentsd az emberiséget? Az izgalmas stratégiai játékban egy járványelhárító csapat szakképzett tagjaként feladatod, hogy felfedezd a halálos járvány ellenszérumát, még mielőtt az világszerte elterjedne…",
                8, new ArrayList<>(List.of(Category.STRATEGY)), new Limits(2,4), new Limits(45,60));
        Game game3 = new Game( "Monopoly", "Vannak azért sokkal sokkal jobb társasok",
                12, new ArrayList<>(List.of(Category.BUILDING)), new Limits(2,6), new Limits(60,120));

        gameRepository.save(game1);
        gameRepository.save(game2);
        gameRepository.save(game3);

        Player user1 = new Player("nagys", "Nagy Sándor", "ns-secret", "nagy.sandor@gmail.com", new ArrayList<>(List.of(Role.SUPERUSER)),new ArrayList<>());
        Player user2= new Player("horvatha", "Horváth Ádám", "ha-secret", "horvath.adam@gmail.com", new ArrayList<>(Arrays.asList(Role.GROUPADMIN, Role.PLAYER)), new ArrayList<>(Arrays.asList(game1,game2)));
        Player user3= new Player("kovacsp", "Kovács Péter", "kp-secret", "kovacs.peter@gmail.com", new ArrayList<>(List.of(Role.PLAYER)), new ArrayList<>(Arrays.asList(game1)));
        Player user4= new Player("kissi", "Kiss István", "ki-secret", "kiss.istvan@gmail.com", new ArrayList<>(List.of(Role.PLAYER)), new ArrayList<>(List.of(game2)));
        Player user5= new Player("totha", "Tóth András", "ta-secret", "toth.andras@gmail.com", new ArrayList<>(List.of(Role.GROUPADMIN, Role.PLAYER)), new ArrayList<>(List.of(game3)));

        userRepository.save(user1);
        userRepository.save(user2);
        userRepository.save(user3);
        userRepository.save(user4);
        userRepository.save(user5);

        Group group = new Group("Óbudai Informatika Játék Csoport", user2, new ArrayList<>(Arrays.asList(user3)));
        Group group2 = new Group("Monopoly Fanok", user5, new ArrayList<>());

        group.AddToEvent(new Event(LocalDateTime.parse("2022-06-06T18:00:00.000"), "NIK aula", "valamit játszunk"));

        groupRepository.save(group);
        groupRepository.save(group2);

        JoinRequest joinRequest = new JoinRequest(group, user4, JoinRequestState.REQUESTED);

        joinRequestRepository.save(joinRequest);

    }

}
