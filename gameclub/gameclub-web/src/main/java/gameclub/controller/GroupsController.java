package gameclub.controller;

import gameclub.DTOs.GroupDTO;
import gameclub.domain.Group;
import gameclub.domain.Player;
import gameclub.security.UserDetailsPrincipal;
import gameclub.service.IGameClubService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
public class GroupsController {

    @Autowired
    private IGameClubService gameClubService;

    @GetMapping("/memberships")
    public String memberships(Model model){
        Player currentUser = ((UserDetailsPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUser();

        List<Group> groupms = gameClubService.GetUserMembershipsOf(currentUser);
        List<GroupDTO> dtos = new ArrayList<>();

        for (Group group : groupms){
            GroupDTO dto = new GroupDTO();
            dto.setName(group.getName());
            dto.setId(group.getId());
            dtos.add(dto);
        }

        model.addAttribute("memberships", dtos);

        List<Group> groupsOther = gameClubService.GetNotMemberGroupsOf(currentUser);
        List<Group> requested = gameClubService.RequestedGroupsBy(currentUser);
        List<GroupDTO> groupDTOS = new ArrayList<>();

        for(Group group: groupsOther){
            GroupDTO dto = new GroupDTO();
            dto.setId(group.getId());
            dto.setName(group.getName());
            dto.setAdminname(group.getAdmin().getName());
            dto.setNomembers(group.getMembers().size());
            dto.setJoinable(true);
            for(Group reqiestedGroup: requested){
                if(reqiestedGroup.getId() == dto.getId()){
                    dto.setJoinable(false);
                }
            }

            groupDTOS.add(dto);
        }


        model.addAttribute("others", groupDTOS);

        return "groups" ;
    }

    @GetMapping("/joinrequest/{id}")
    public String joinrequest(@PathVariable("id") Long id){
        Player currentUser = ((UserDetailsPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUser();
        gameClubService.CreateJoinRequest(currentUser, id);
        return "redirect:/memberships";
    }
}
