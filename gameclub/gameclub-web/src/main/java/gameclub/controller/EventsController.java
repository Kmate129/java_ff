package gameclub.controller;

import gameclub.DTOs.EventDTO;
import gameclub.DTOs.PlayerDTO;
import gameclub.domain.Event;
import gameclub.domain.Group;
import gameclub.domain.Player;
import gameclub.security.UserDetailsPrincipal;
import gameclub.service.IGameClubService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Controller
public class EventsController {

    @Autowired
    IGameClubService gameClubService;

    @GetMapping("/events/{id}")
    public String events(Model model, @PathVariable("id") Long groupId){
        Group group = gameClubService.GetGroupById(groupId);

        List<EventDTO> dtos = new ArrayList<>();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        for(Event event:group.getEvents()){
            EventDTO dto = new EventDTO();
            dto.setDate(event.getDate().format(formatter));
            dto.setPlace(event.getPlace());
            dto.setDescription(event.getDescription());
            dto.setParticipants(new ArrayList<>());
            for(Player participant: event.getParticipants()){
                PlayerDTO playerDTO = new PlayerDTO();
                playerDTO.setName(participant.getName());

                dto.getParticipants().add(playerDTO);
            }

            dtos.add(dto);
        }

        model.addAttribute("events", dtos);

        EventDTO newEvent = new EventDTO();

        model.addAttribute("event", newEvent);

        return "events-list";
    }

    @PostMapping("/addevent")
    public String addEventForm(@Valid EventDTO eventDTO, BindingResult bindingResult){
        Player currentUser = ((UserDetailsPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUser();

        if(bindingResult.hasErrors()){
            return "redirect:/events/" + this.gameClubService.GetOwnGroupOf(currentUser).getId();
        }

        Event event = new Event(LocalDateTime.parse(eventDTO.getDate()), eventDTO.getPlace(), eventDTO.getDescription());

        gameClubService.AddEventToList(currentUser, event);

        return "redirect:/events/" + this.gameClubService.GetOwnGroupOf(currentUser).getId();
    }
}
