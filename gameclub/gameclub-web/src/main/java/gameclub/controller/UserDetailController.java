package gameclub.controller;

import gameclub.DTOs.PlayerDTO;
import gameclub.domain.Player;
import gameclub.security.Role;
import gameclub.security.UserDetailsPrincipal;
import gameclub.service.IGameClubService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class UserDetailController {

    @GetMapping("/")
    public String home(){
        var currentUser = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        if(currentUser.contains(Role.ROLE_SUPERUSER)){
            return "redirect:gameslist";
        }

        return "redirect:userdetails";
    }

    @GetMapping("/userdetails")
    public String userDetails(Model model){
        PlayerDTO user = new PlayerDTO();

        Player currentUser = ((UserDetailsPrincipal)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUser();
        user.setName( currentUser.getName());
        user.setLoginName( currentUser.getLoginName());
        user.setEmail( currentUser.getEmail());

        model.addAttribute("user", user);

        return "user-details";
    }
}
