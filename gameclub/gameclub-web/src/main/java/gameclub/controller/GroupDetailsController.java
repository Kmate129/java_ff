package gameclub.controller;

import gameclub.DTOs.EventDTO;
import gameclub.DTOs.GroupDetailDTO;
import gameclub.DTOs.PlayerDTO;
import gameclub.domain.Event;
import gameclub.domain.Group;
import gameclub.domain.Player;
import gameclub.security.UserDetailsPrincipal;
import gameclub.service.IGameClubService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Controller
public class GroupDetailsController {

    @Autowired
    private IGameClubService gameClubService;

    @GetMapping("/groupdetails/{id}")
    public String groupDetails(Model model ,@PathVariable("id") Long id){
        Player currentUser = ((UserDetailsPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUser();
        Group group = gameClubService.GetGroupById(id);

        GroupDetailDTO dto = new GroupDetailDTO();
        dto.setId(group.getId());
        dto.setName(group.getName());
        dto.setAdminname(group.getAdmin().getName());

        List<PlayerDTO> members = new ArrayList<>();
        for(Player member: group.getMembers()){
            PlayerDTO toAdd = new PlayerDTO();
            toAdd.setName(member.getName());
            toAdd.setEmail(member.getEmail());
            members.add(toAdd);
        }
        dto.setMembers(members);

        List<EventDTO> events = new ArrayList<>();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        for(Event event: group.getEvents()){
            EventDTO toAdd = new EventDTO();
            toAdd.setId(event.getId());
            toAdd.setDate(event.getDate().format(formatter));
            toAdd.setPlace(event.getPlace());
            if(gameClubService.IsPlayerAttendsOnEvent(currentUser, event.getId())){
                toAdd.setAttend(false);
            }else{
                toAdd.setAttend(true);
            }
            events.add(toAdd);
        }
        dto.setEvents(events);

        model.addAttribute("group", dto);

        return "group-details";
    }

    @GetMapping("/attend/{id}")
    public String attend(@PathVariable("id") Long id){
        Player currentUser = ((UserDetailsPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUser();
        gameClubService.PlayerAttendsOnEvent(currentUser, id);

        return "redirect:/groupdetails/" + gameClubService.GetGroupByEventId(id).getId();
    }
}
