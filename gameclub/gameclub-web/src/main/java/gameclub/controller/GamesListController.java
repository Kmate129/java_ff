package gameclub.controller;

import gameclub.DTOs.GameDTO;
import gameclub.domain.Category;
import gameclub.domain.Game;
import gameclub.domain.Player;
import gameclub.domain.Role;
import gameclub.security.UserDetailsPrincipal;
import gameclub.service.IGameClubService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.ArrayList;
import java.util.List;

@Controller
public class GamesListController {

    @Autowired
    private IGameClubService gameClubService;

    @GetMapping("/gameslist")
    public String gamesList(Model model) {
        Player currentUser = ((UserDetailsPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUser();

        if(!currentUser.getRole().equals(Role.SUPERUSER)){
            List<GameDTO> mygames = new ArrayList<>();

            for (Game game : currentUser.getGames()) {
                GameDTO dto = new GameDTO();
                dto.setName(game.getName());
                dto.setDescription(game.getDescription());
                dto.setMinage(game.getMinimumAge());
                dto.setPlayersnumber(game.getNumberOfPlayers().getMin() + " - " + game.getNumberOfPlayers().getMax());
                dto.setPlaytime(game.getPlayTime().getMin() + " - " + game.getPlayTime().getMax() + " min");
                dto.setCategories(CatToString(game.getCategories()));

                mygames.add(dto);
            }

            model.addAttribute("mygames", mygames);
        }

        List<GameDTO> gameDTOs = new ArrayList<>();
        List<Game> games = gameClubService.GetAllGames();

        for (Game game : games) {
            GameDTO dto = new GameDTO();
            dto.setId(game.getId());
            dto.setName(game.getName());
            dto.setDescription(game.getDescription());
            dto.setMinage(game.getMinimumAge());
            dto.setPlayersnumber(game.getNumberOfPlayers().getMin() + " - " + game.getNumberOfPlayers().getMax());
            dto.setPlaytime(game.getPlayTime().getMin() + " - " + game.getPlayTime().getMax() + " min");
            dto.setCategories(CatToString(game.getCategories()));
            if(!currentUser.getRole().equals(Role.SUPERUSER)){
                dto.setAction(true);
                List<Game> mygames = currentUser.getGames();
                for (Game current: mygames){
                    if(current.getId() == game.getId()){
                        dto.setAction(false);
                    }
                }
            }

            gameDTOs.add(dto);
        }

        model.addAttribute("games", gameDTOs);

        return "games-list";
    }

    @GetMapping("/gameslist/add/{id}")
    public String gamesList(@PathVariable("id") long id) {
        Player currentUser = ((UserDetailsPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUser();
        Game toAdd = gameClubService.GetGameById(id);
        gameClubService.AddGameToOwnedGamesOf(currentUser, toAdd);
        return "redirect:/gameslist";
    }

    private String CatToString(List<Category> list){
        String retVal="";
        for (int i=0; i<list.size(); i++){
            retVal+=list.get(i);
            if(i< list.size()-1){
                retVal+=", ";
            }
        }

        return retVal;
    }

}