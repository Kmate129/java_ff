package gameclub.controller;

import gameclub.DTOs.GroupDetailDTO;
import gameclub.DTOs.JoinRequestDTO;
import gameclub.DTOs.PlayerDTO;
import gameclub.domain.Group;
import gameclub.domain.JoinRequest;
import gameclub.domain.JoinRequestState;
import gameclub.domain.Player;
import gameclub.security.UserDetailsPrincipal;
import gameclub.service.IGameClubService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import java.util.ArrayList;
import java.util.List;

@Controller
public class MyGroupDetailsController {

    @Autowired
    private IGameClubService gameClubService;

    @GetMapping("/mygroup")
    public String mygroup(Model model) {
        Player currentUser = ((UserDetailsPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUser();
        Group ownGroup = gameClubService.GetOwnGroupOf(currentUser);

        GroupDetailDTO dto = new GroupDetailDTO();
        dto.setId(ownGroup.getId());
        dto.setName(ownGroup.getName());
        List<PlayerDTO> members = new ArrayList<>();
        for(Player player: ownGroup.getMembers()){
            PlayerDTO playerDTO = new PlayerDTO();
            playerDTO.setEmail(player.getEmail());
            playerDTO.setName(player.getName());

            members.add(playerDTO);
        }

        dto.setMembers(members);

        List<JoinRequestDTO> requests = new ArrayList<>();
        for(JoinRequest request: gameClubService.GetRequestsOf(ownGroup.getId())){
            JoinRequestDTO jrDTO = new JoinRequestDTO();
            jrDTO.setName(request.getPlayer().getName());
            jrDTO.setPlayerid(request.getPlayer().getId());

            requests.add(jrDTO);
        }

        dto.setRequests(requests);

        model.addAttribute("group", dto);

        return "mygroup-details";
    }

    @GetMapping("/mygroup/accepting/{id}")
    public String mygroup(@PathVariable("id") long playerId) {
        var a = gameClubService.GetAllGroups();
        var b = gameClubService.GetAllRequests();

        Player currentUser = ((UserDetailsPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUser();
        gameClubService.HandleJoinRequest(currentUser, playerId, JoinRequestState.ACCEPTED);

        return "redirect:/mygroup";
    }

    @GetMapping("/mygroup/rejecting/{id}")
    public String reject(@PathVariable("id") Long playerId) {
        Player currentUser = ((UserDetailsPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUser();
        gameClubService.HandleJoinRequest(currentUser, playerId, JoinRequestState.REJECTED);
        return "redirect:/mygroup";
    }
}
