package gameclub.controller;

import gameclub.DTOs.GameFormDTO;
import gameclub.domain.Category;
import gameclub.domain.Game;
import gameclub.domain.Limits;
import gameclub.service.IGameClubService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
public class AddGameController {

    @Autowired
    private IGameClubService gameClubService;

    @GetMapping("/addgameform")
    public String addGame(Model model){
        GameFormDTO newGame = new GameFormDTO();
        List<String> cats = new ArrayList<>();
        Arrays.stream(Category.values()).forEach(x->cats.add(x.toString()));

        model.addAttribute("categories", cats);
        model.addAttribute("game", newGame);

        return "add-game-form";
    }

    @PostMapping("/addgame")
    public String addGameForm(@Valid GameFormDTO gameDTO, BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            return "redirect:addgameform";
        }

        Game game = new Game(
                gameDTO.getName(),
                gameDTO.getDescription(),
                gameDTO.getMinimumage(),
                new ArrayList<>(Arrays.asList(gameDTO.getCategory())),
                new Limits(gameDTO.getMinplaytime(), gameDTO.getMaxplaytime()),
                new Limits(gameDTO.getMinplayer(), gameDTO.getMaxplaytime())
        );

        gameClubService.AddGameToRepository(game);
        
        return "redirect:/addgameform";
    }
}
