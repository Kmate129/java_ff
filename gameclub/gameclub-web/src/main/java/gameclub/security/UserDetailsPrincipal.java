package gameclub.security;

import gameclub.domain.Player;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import java.util.Collection;
import java.util.HashSet;

public class UserDetailsPrincipal implements UserDetails {
    final private Player user;

    public UserDetailsPrincipal(Player user){
        this.user=user;
    }

    public Player getUser(){
        return user;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Collection<Role> auth = new HashSet<>();
        for(gameclub.domain.Role role: user.getRole()){
            if(role.equals(gameclub.domain.Role.SUPERUSER)){
                auth.add(Role.ROLE_SUPERUSER);
            }
            if(role.equals(gameclub.domain.Role.GROUPADMIN)){
                auth.add(Role.ROLE_GROUPADMIN);
            }
            if(role.equals(gameclub.domain.Role.PLAYER)){
                auth.add(Role.ROLE_PLAYER);
            }
        }

        return auth;
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getLoginName();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
