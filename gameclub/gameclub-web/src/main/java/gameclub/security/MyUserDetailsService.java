package gameclub.security;

import gameclub.domain.Player;
import gameclub.security.UserDetailsPrincipal;
import gameclub.service.IGameClubService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class MyUserDetailsService implements UserDetailsService {

    @Autowired
    public IGameClubService gameClubService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Player user = gameClubService.GetUserByLoginName(username);
        if(user==null){
            throw new UsernameNotFoundException(username);
        }

        return new UserDetailsPrincipal(user);
    }
}
