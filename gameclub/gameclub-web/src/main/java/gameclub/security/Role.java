package gameclub.security;

import org.springframework.security.core.GrantedAuthority;

public enum Role implements GrantedAuthority {

    ROLE_SUPERUSER, ROLE_GROUPADMIN, ROLE_PLAYER;

    @Override
    public String getAuthority() {
        return name();
    }
}
