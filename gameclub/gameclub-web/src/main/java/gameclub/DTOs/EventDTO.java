package gameclub.DTOs;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;
import gameclub.DTOs.Constraints.DateTimeConstraint;

@Data
public class EventDTO {
    private Long id;

    @DateTimeConstraint
    private String date;

    @NotEmpty(message = "Location can not be empty")
    private String place;

    @NotEmpty(message = "Description can not be empty")
    private String description;
    private List<PlayerDTO> participants;
    private boolean attend;
}
