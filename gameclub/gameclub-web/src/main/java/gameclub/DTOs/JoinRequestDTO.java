package gameclub.DTOs;

import lombok.Data;

@Data
public class JoinRequestDTO {
    private long playerid;
    private String name;
}
