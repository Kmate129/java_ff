package gameclub.DTOs;

import lombok.Data;

@Data
public class GroupDTO {
    private long id;
    private String name;
    private String adminname;
    private int nomembers;
    private boolean joinable;
}
