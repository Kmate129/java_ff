package gameclub.DTOs;

import gameclub.domain.Event;
import gameclub.domain.Player;
import lombok.Data;
import java.util.List;

@Data
public class GroupDetailDTO {
    private long id;
    private String name;
    private String adminname;
    private List<PlayerDTO> members;
    private List<EventDTO> events;
    private List<JoinRequestDTO> requests;
}
