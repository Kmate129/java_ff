package gameclub.DTOs;

import lombok.Data;

@Data
public class GameDTO {
    private long id;
    private String name;
    private String description;
    private int minage;
    private String playersnumber;
    private String playtime;
    private String categories;
    private boolean action;
}
