package gameclub.DTOs;

import gameclub.domain.Category;
import gameclub.DTOs.Constraints.PlayernumConstraint;
import gameclub.DTOs.Constraints.PlaytimeConstraint;
import lombok.Data;
import javax.validation.constraints.*;

@Data
@PlayernumConstraint
@PlaytimeConstraint
public class GameFormDTO {

    @NotEmpty(message = "Name can not be empty")
    private String name;

    @NotEmpty(message = "Description can not be empty")
    private String description;

    @Min(value = 3, message = "Minimum age must be 3 or greater")
    private int minimumage;

    @PositiveOrZero(message = "Minimum player number can not be negative")
    private int minplayer;

    private int maxplayer;

    @PositiveOrZero(message = "Minimum playtime can not be negative")
    private int minplaytime;

    private int maxplaytime;

    @NotNull
    private Category category;
}
