package gameclub.DTOs;

import lombok.Data;

@Data
public class PlayerDTO {
    private String name;
    private String loginName;
    private String email;
}
