package gameclub.DTOs.Constraints;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = PlaytimeValidator.class)
@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface PlaytimeConstraint {
    String message() default "Max playtime must be greater than min playtime";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
