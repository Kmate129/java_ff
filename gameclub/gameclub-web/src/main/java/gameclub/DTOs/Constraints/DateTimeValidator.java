package gameclub.DTOs.Constraints;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDateTime;

public class DateTimeValidator implements ConstraintValidator<DateTimeConstraint, String> {

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return LocalDateTime.now().isBefore(LocalDateTime.parse(value));
    }
}
