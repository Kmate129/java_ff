package gameclub.DTOs.Constraints;

import gameclub.DTOs.Constraints.PlayernumConstraint;
import gameclub.DTOs.GameFormDTO;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PlayernumValidator implements ConstraintValidator<PlayernumConstraint, GameFormDTO> {

    @Override
    public boolean isValid(GameFormDTO value, ConstraintValidatorContext context) {
        return value.getMaxplayer() > value.getMinplayer();
    }

}
