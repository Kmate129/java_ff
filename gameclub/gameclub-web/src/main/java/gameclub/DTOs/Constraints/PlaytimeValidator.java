package gameclub.DTOs.Constraints;

import gameclub.DTOs.Constraints.PlaytimeConstraint;
import gameclub.DTOs.GameFormDTO;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PlaytimeValidator implements ConstraintValidator<PlaytimeConstraint, GameFormDTO> {

    @Override
    public boolean isValid(GameFormDTO value, ConstraintValidatorContext context) {
        return value.getMaxplaytime() > value.getMinplaytime();
    }

}
