package gameclub.DTOs.Constraints;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = PlayernumValidator.class)
@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface PlayernumConstraint {
    String message() default "Max player number must be greater than min player number";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
