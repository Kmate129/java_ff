package gameclub.domain;

import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import java.util.Objects;

@Embeddable
@NoArgsConstructor(force = true)
public class Limits {
	private final int min;
	private final int max;
	
	public Limits(int min, int max) {
		super();
		this.min = min;
		this.max = max;
	}

	public int getMin() {
		return min;
	}

	public int getMax() {
		return max;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Limits limits = (Limits) o;
		return min == limits.min && max == limits.max;
	}

	@Override
	public int hashCode() {
		return Objects.hash(min, max);
	}
}
