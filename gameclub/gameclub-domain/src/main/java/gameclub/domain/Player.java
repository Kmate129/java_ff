package gameclub.domain;

import lombok.NoArgsConstructor;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name="players")
@NoArgsConstructor(force = true)
public class Player extends User {

	@ManyToMany(fetch = FetchType.EAGER)
	private List<Game> games;

	public Player(String loginName, String name, String password, String email, ArrayList<Role> role,	ArrayList<Game> games) {
		super(loginName, name, password, email, role);
		this.games=games;
	}

	public List<Game> getGames() {
		return games;
	}

	public void AddToGames(Game game){
		if(games != null){
			games.add(game);
		}else{
			games = new ArrayList<>();
			games.add(game);
		}
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		return super.equals(o);
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), games);
	}
}
