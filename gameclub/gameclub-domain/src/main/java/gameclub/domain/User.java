package gameclub.domain;

import lombok.NoArgsConstructor;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@MappedSuperclass
@NoArgsConstructor(force = true)
public abstract class User {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private final long id;
	private final String loginName;
	private final String name;
	private final String password;
	private final String email;

	@ElementCollection
	@LazyCollection(LazyCollectionOption.FALSE)
	private final List<Role> role;
	
	public User(String loginName, String name, String password, String email, ArrayList<Role> role) {
		super();
		this.id = 0;
		this.loginName = loginName;
		this.name = name;
		this.password = password;
		this.email = email;
		this.role = role;
	}

	public String getLoginName() {
		return loginName;
	}

	public String getName() {
		return name;
	}

	public String getPassword() {
		return password;
	}

	public String getEmail() {
		return email;
	}

	public List<Role> getRole() {
		return role;
	}

	public long getId() {
		return id;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		User user = (User) o;
		return id == user.id;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, loginName, name, password, email, role);
	}
}


