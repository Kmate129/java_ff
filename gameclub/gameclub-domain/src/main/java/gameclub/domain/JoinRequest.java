package gameclub.domain;

import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name="joinrequests")
@NoArgsConstructor(force = true)
public class JoinRequest {

	@Id
	private JoinRequestId id;

	@Enumerated(EnumType.STRING)
	private JoinRequestState state;
	
	public JoinRequest(Group group, Player player, JoinRequestState state) {
		super();
		this.id= new JoinRequestId(group, player);
		this.state = state;
	}

	public Group getGroup() {
		return id.getGroup();
	}

	public Player getPlayer() {
		return id.getPlayer();
	}

	public JoinRequestState getState() {
		return state;
	}

	public void setState(JoinRequestState state) {
		this.state = state;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		JoinRequest that = (JoinRequest) o;
		return Objects.equals(id, that.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, state);
	}
}
