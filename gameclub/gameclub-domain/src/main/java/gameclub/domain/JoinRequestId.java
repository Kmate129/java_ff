package gameclub.domain;

import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import javax.persistence.OneToOne;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
@NoArgsConstructor(force = true)
public class JoinRequestId implements Serializable {

    @OneToOne
    private Group group;

    @OneToOne
    private Player player;

    public JoinRequestId(Group group, Player player){
        this.group=group;
        this.player=player;
    }

    public Group getGroup() {
        return group;
    }

    public Player getPlayer() {
        return player;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JoinRequestId that = (JoinRequestId) o;
        return Objects.equals(group, that.group) && Objects.equals(player, that.player);
    }

    @Override
    public int hashCode() {
        return Objects.hash(group, player);
    }
}
