package gameclub.domain;

import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name="events")
@NoArgsConstructor(force = true)
public class Event {

	@Id
	@GeneratedValue
	private long id;
	private LocalDateTime date;
	private String place;
	private String description;

	@ManyToMany
	private List<Player> participants;
	
	public Event(LocalDateTime date, String place, String description) {
		super();
		this.id=0;
		this.date = date;
		this.place = place;
		this.description=description;
		this.participants = new ArrayList<>();
	}

	public void AddToParticipants(Player player){
		participants.add(player);
	}

	public long getId(){
		return id;
	}

	public LocalDateTime getDate(){
		return date;
	}

	public String getPlace(){
		return place;
	}

	public String getDescription(){
		return description;
	}

	public List<Player> getParticipants(){
		return participants;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Event event = (Event) o;
		return id == event.id;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, date, place, participants);
	}
}
