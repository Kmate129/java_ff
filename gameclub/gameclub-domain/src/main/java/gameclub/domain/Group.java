package gameclub.domain;

import lombok.NoArgsConstructor;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name="groups")
@NoArgsConstructor(force = true)
public class Group {

	@Id
	@GeneratedValue
	private long id;
	private String name;

	@OneToOne
	private Player admin;

	@ManyToMany()
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<Player> members;

	@OneToMany(cascade=CascadeType.ALL)
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<Event> events;

	public Group(String name, Player admin, ArrayList<Player> members) {
		super();
		this.id=0;
		this.name=name;
		this.admin=admin;
		this.members=members;
		this.events=new ArrayList<>();
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Player getAdmin() {
		return admin;
	}

	public List<Player> getMembers() {
		return members;
	}

	public List<Event> getEvents() {
		return events;
	}

	public void AddToMembers(Player player){
		members.add(player);
	}

	public void AddToEvent(Event event) { events.add(event); }

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Group group = (Group) o;
		return id == group.id;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, name, admin, members, events);
	}
}
