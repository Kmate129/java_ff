package gameclub.domain;

import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name="games")
@NoArgsConstructor(force = true)
public final class Game {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private final long id;
	private final String name;
	private final String description;
	private final int minimumAge;

	@ElementCollection(fetch = FetchType.EAGER)
	private final List<Category> categories;

	@Embedded
	@AttributeOverrides({
			@AttributeOverride( name = "min", column = @Column(name = "playtime_min")),
			@AttributeOverride( name = "max", column = @Column(name = "playtime_max")),
	})
	private final Limits playTime;

	@Embedded
	@AttributeOverrides({
			@AttributeOverride( name = "min", column = @Column(name = "numberOfPlayers_min")),
			@AttributeOverride( name = "max", column = @Column(name = "numberOfPlayers_max")),
	})
	private final Limits numberOfPlayers;

	@ManyToMany(mappedBy = "games")
	private final List<Player> players;

	public Game(String name, String description, int minimumAge, ArrayList<Category> categories,
			Limits playTime, Limits numberOfPlayers) {
		super();
		this.id=0;
		this.name = name;
		this.description = description;
		this.minimumAge = minimumAge;
		this.categories = categories;
		this.playTime = playTime;
		this.numberOfPlayers = numberOfPlayers;
		this.players=new ArrayList<>();
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public int getMinimumAge() {
		return minimumAge;
	}

	public List<Category> getCategories() {
		return categories;
	}

	public Limits getPlayTime() {
		return playTime;
	}

	public Limits getNumberOfPlayers() {
		return numberOfPlayers;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Game game = (Game) o;
		return id == game.id;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, name, description, minimumAge, categories, playTime, numberOfPlayers, players);
	}
}
