package gameclub.domain;

public enum Role {
	SUPERUSER,
	GROUPADMIN,
	PLAYER
}
