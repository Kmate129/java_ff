package gameclub.service;

import gameclub.domain.*;

import java.util.List;
import java.util.NoSuchElementException;

public interface IGameClubService {
    List<Group> GetUserMembershipsOf(Player user);
    List<Group> GetNotMemberGroupsOf(Player user);
    List<Group> RequestedGroupsBy(Player user);
    Group GetGroupById(Long id);
    List<Game> GetAllGames();
    Game GetGameById(Long id);
    void AddGameToRepository(Game game);
    void AddEventToList(Player player, Event event);
    void CreateJoinRequest(Player user, Long group);
    Group GetOwnGroupOf(Player user) throws NoSuchElementException;
    void PlayerAttendsOnEvent(Player player, Long eventId);
    boolean IsPlayerAttendsOnEvent(Player player, Long eventId);
    Group GetGroupByEventId(Long eventId);
    void AddGameToOwnedGamesOf(Player currentUser, Game game);
    void HandleJoinRequest(Player currentUser, Long playerid, JoinRequestState requestResult);
    List<JoinRequest> GetRequestsOf(Long id);
    Player GetUserByLoginName(String loginname);
    List<JoinRequest> GetAllRequests();
    List<Group> GetAllGroups();
}
