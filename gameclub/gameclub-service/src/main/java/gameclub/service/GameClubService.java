package gameclub.service;

import com.google.common.collect.Lists;
import gameclub.domain.*;
import gameclub.persistence.repositories.IGameRepository;
import gameclub.persistence.repositories.IGroupRepository;
import gameclub.persistence.repositories.IJoinRequestRepository;
import gameclub.persistence.repositories.IUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
@Service
public class GameClubService implements IGameClubService{

    @Autowired
    private IGameRepository gameRepository;

    @Autowired
    private IGroupRepository groupRepository;

    @Autowired
    private IJoinRequestRepository joinRequestRepository;

    @Autowired
    private IUserRepository userRepository;


    public List<Group> GetUserMembershipsOf(Player user){
        return groupRepository.findAll().stream().filter(x->x.getMembers().contains(user)).collect(Collectors.toList());
    }

    public List<Group> GetNotMemberGroupsOf(Player user){
        return groupRepository.findAll().stream().filter(x->!x.getMembers().contains(user) && !x.getAdmin().equals(user)).collect(Collectors.toList());
    }

    public Group GetOwnGroupOf(Player user) throws NoSuchElementException {
        return groupRepository.findAll().stream().filter(x->x.getAdmin().equals(user)).findFirst().orElseThrow(NoSuchElementException::new);
    }

    public List<Game> GetAllGames(){
        return Lists.newArrayList(gameRepository.findAll());
    }

    public void AddGameToOwnedGamesOf(Player currentUser, Game game){
        currentUser.AddToGames(game);
        userRepository.save(currentUser);
    }

    public void AddGameToRepository(Game game){
        gameRepository.save(game);
    }

    @Override
    public void AddEventToList(Player player, Event event) {
        Group group = this.GetOwnGroupOf(player);
        group.AddToEvent(event);
        groupRepository.save(group);
    }

    public Group GetGroupById(Long id){
        return groupRepository.findById(id).get();
    }

    public void CreateJoinRequest(Player user, Long group){
        Group requested = groupRepository.findById(group).get();
        if(requested!=null){
            joinRequestRepository.save(new JoinRequest(requested, user, JoinRequestState.REQUESTED));
        }
    }

    public void HandleJoinRequest(Player currentUser, Long playerid, JoinRequestState requestResult){
         JoinRequest request = Objects.requireNonNull(joinRequestRepository.findAll().stream().filter(x -> x.getGroup().equals(GetOwnGroupOf(currentUser)) && x.getPlayer().getId() == playerid)
                .findFirst().orElse(null));
         request.setState(requestResult);
        joinRequestRepository.save(request);

        if(requestResult.equals(JoinRequestState.ACCEPTED)){
            var user = (Player)userRepository.findAll().stream().filter(x->x.getId()==playerid).findFirst().orElse(null);
            Group group = Objects.requireNonNull(groupRepository.findAll().stream().filter(x -> x.getAdmin().equals(currentUser)).findFirst().orElse(null));
            group.AddToMembers(user);
            groupRepository.save(group);
        }
    }

    public List<JoinRequest> GetRequestsOf(Long id){
        var owngroup = groupRepository.findById(id).get();
        var requests =joinRequestRepository.findAll();
        return requests.stream().filter(x->x.getGroup().equals(owngroup) && x.getState().equals(JoinRequestState.REQUESTED)).collect(Collectors.toList());
    }

    public Player GetUserByLoginName(String loginname){
        return userRepository.findByLoginName(loginname);
    }

    @Override
    public List<JoinRequest> GetAllRequests() {
        return joinRequestRepository.findAll();
    }

    @Override
    public List<Group> GetAllGroups() {
        return groupRepository.findAll();
    }

    public List<Group> RequestedGroupsBy(Player user){
        var requests = joinRequestRepository.findAll().stream().filter(x->x.getPlayer().equals(user) && x.getState().equals(JoinRequestState.REQUESTED)).collect(Collectors.toList());
        List<Group> retVal = new ArrayList<>();
        for(JoinRequest jr: requests){
            retVal.add(jr.getGroup());
        }

        return retVal;
    }

    public Game GetGameById(Long id){
        return gameRepository.findById(id).get();
    }

    public void PlayerAttendsOnEvent(Player player, Long eventId){
        for(Group group: this.GetAllGroups()){
            for(Event event: group.getEvents()){
                if(eventId.equals(event.getId())){
                    event.AddToParticipants(player);
                    groupRepository.save(group);
                    return;
                }
            }
        }
    }

    public boolean IsPlayerAttendsOnEvent(Player player, Long eventId){
        for(Group group: this.groupRepository.findAll()){
            for(Event event: group.getEvents()){
                if(eventId.equals(event.getId()) && event.getParticipants().contains(player)){
                    return true;
                }
            }
        }

        return false;
    }

    public Group GetGroupByEventId(Long eventId){
        for(Group group: this.groupRepository.findAll()){
            for(Event event: group.getEvents()){
                if(eventId.equals(event.getId())){
                    return group;
                }
            }
        }

        throw new NoSuchElementException();
    }
}
